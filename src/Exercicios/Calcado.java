package Exercicios;

public class Calcado {
	private int numero;
	private String material;
	private String empresa;
	private float pre�o;
	
	public Calcado(int numero) {
		this.numero = numero;
	}
	
	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public String getMaterial() {
		return material;
	}

	public void setMaterial(String material) {
		this.material = material;
	}

	public String getEmpresa() {
		return empresa;
	}

	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}

	public float getPre�o() {
		return pre�o;
	}

	public void setPre�o(float pre�o) {
		this.pre�o = pre�o;
	}

	public void cal�ar() {
		System.out.println("Vamos colocar o cal�ado. ");
	}
	
	public void fabricar() {
		System.out.println("Foi fabricado no Brasil. ");
	}
	
	public void retirar() {
		System.out.println("Vamos retirar o cal�ado. ");
	}
	
	public void comprar() {
		System.out.println("Comprei na loja. ");
	}

}
