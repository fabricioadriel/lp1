package Exercicios;

public class Lugar {
	private String lugares;
	private String cidade;
	public int qtd_pessoas;
	
	public Lugar(String cidade) {
		this.cidade = cidade;
	}
	
	public void setCidade(String cid) {
		cidade = cid;
	}
	
	public String getCidade() {
		return cidade;
	}
	
	public void setLugares(String lug) {
		lugares = lug;
	}
	
	public String getLugares() {
		return lugares;
	}
	
	public void visitar() {
		System.out.println("Vamos sair de mascaras");
	}
	
	public void sair() {
		System.out.println("Sair apenas se necess�rio");
	}
	
	public void permiss�o() {
		System.out.println("Cuidado com a quantidade de pessoas no mesmo local");
	}

}

