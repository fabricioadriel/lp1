package Exercicios;

public class Pessoa {
	private String nome;
	private int idade;
	private String cidade;
	
	public Pessoa(String nome, int idade) {}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getIdade() {
		return idade;
	}

	public void setIdade(int idade) {
		this.idade = idade;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	
	public void ficarQuarentena() {
		System.out.println("Se tiver como, fique em casa");
	}
	
	public void viajar() {
		System.out.println("Depois da quarentena, vamos viajar");
	}
	

}
