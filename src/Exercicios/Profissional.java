package Exercicios;

public class Profissional {
	private String forma��o;
	private String empresa;
	private String institui��o;
	
	public Profissional(String forma��o) {
		this.forma��o = forma��o;
	}

	public String getForma��o() {
		return forma��o;
	}

	public void setForma��o(String forma��o) {
		this.forma��o = forma��o;
	}

	public String getEmpresa() {
		return empresa;
	}

	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}

	public String getInstitui��o() {
		return institui��o;
	}

	public void setInstitui��o(String institui��o) {
		this.institui��o = institui��o;
	}
	
	public void Qualificar() {
		System.out.println("Estou estudando na Fatec.");
	}
	
	public void Trabalhar() {
		System.out.println("Em tempo de pandemia, tem muitos profissionais em Home Office.");
	}
	
	public void TrocarEmprego() {
		System.out.println("Estou trabalhando em outra empresa. ");
	}

}
