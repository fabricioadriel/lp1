package Exercicios;

public class Instrument {
	private String tipo;
	private String nome;
	private String material;
	
	public Instrument(String nome) {
		this.nome = nome;
		
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	public String getTipo() {
		return tipo;
	}
	
	
	
	public void setMaterial(String material) {
		this.material = material;
	}
	
	public String getMaterial() {
		return material;
	}
	
	public void tocar() {
		System.out.println("Vai come�ar o show");
	}
	
	public void ligar_equipamento() {
		System.out.println("Depende do tipo de seu equipamento");
	}
	
	public void desligar_equipamento() {
		System.out.println("O show nunca termina, apenas adorme.");
	}

}
