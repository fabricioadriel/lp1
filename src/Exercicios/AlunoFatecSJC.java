package Exercicios;

public class AlunoFatecSJC {
	private String materia;
	private float notas;
	private int semestre;
	
	public AlunoFatecSJC (int semestre) {
		this.semestre = semestre;
	}
	
	public void setMateria(String materia) {
		this.materia = materia;
	}
	
	public String getMateria() {
		return materia;
	}
	
	public void setNotas(float notas) {
		this.notas = notas;
	}
	
	public float getNotas() {
		return notas;
	}
	
	public void setSemestre(int semestre) {
		this.semestre = semestre;
	}
	
	public int getSemestre() {
		return semestre;
	}
	
	public void fazerProva() {
		System.out.println("Voce obteve no minimo m�dia 6.");
	}
	
	public void irFaculdade() {
		System.out.println("Presen�a de no minimo 75%");
	}
	
	public void retirarLivro() {
		System.out.println("Voce ja retirou livro na biblioteca");
	}
	public void terminar() {
		System.out.println("Parab�ns voce terminou os Estudos.");
	}
}
